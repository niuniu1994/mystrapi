'use strict';

/**
 * moto-catalog service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::moto-catalog.moto-catalog');
