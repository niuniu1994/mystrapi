'use strict';

/**
 * moto-catalog router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::moto-catalog.moto-catalog');
