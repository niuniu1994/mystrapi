'use strict';

/**
 * moto-catalog controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::moto-catalog.moto-catalog');
